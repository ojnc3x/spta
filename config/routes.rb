Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users,
    controllers: {
      sessions: 'users/sessions',
      passwords: 'users/passwords'
    }

  namespace :admin do
    root to: "dashboard#index"
    resources :banners
    resources :pages
    resources :projects
    resources :articles
    resources :events
    resources :apps
    resources :profile, only: [:edit, :update]
    resources :press_resources
    resources :attachments
    resources :attachment_categories
  end


  namespace :api do

    resources :attachments do
      member do
        get :download
      end
    end

  end
  #end


  resources :pages, only: [:index, :show] do
  #  resources :elements, only: [:create, :update, :destroy]
  end
  resources :projects, only: [:index, :show]
  resources :articles, only: [:index, :show]
  resources :events, only: [:index, :show]
  resources :apps, only: [:index, :show]
  resources :press_resources, only: [:index, :show]
  resources :emblematic_cases, only: [:index, :show]
  resources :attachments, only: [:index, :show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
end
