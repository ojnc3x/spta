class CreateElements < ActiveRecord::Migration[5.0]
  def change
    create_table :elements do |t|
      t.references :elementable, polymorphic: true, index: true
      t.string :title
      t.string :columns
      t.integer :order
      t.text :content
      t.timestamps
    end
  end
end
