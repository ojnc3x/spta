class CreateAudios < ActiveRecord::Migration[5.0]
  def change
    create_table :audios do |t|
      t.references :audio_element
      t.string :title
      t.string :description
      t.string :url
      t.attachment :file
      t.timestamps
    end
  end
end
