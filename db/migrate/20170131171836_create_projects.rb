class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :title
      t.string :slug, index: true, uniq: true
      t.string :description
      t.integer :order
      t.integer :page_id
      t.integer :feature
      t.attachment :image
      t.timestamps
    end
  end
end
