class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
      t.references :attachment_category
      t.string :name
      t.text :content
      t.integer :downloads, null: false, default: 0
      t.attachment :thumbnail
      t.attachment :datafile
      t.timestamps
    end
  end
end
