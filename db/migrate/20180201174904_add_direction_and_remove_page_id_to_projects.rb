class AddDirectionAndRemovePageIdToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :direction, :integer
    remove_column :projects, :page_id
  end
end
