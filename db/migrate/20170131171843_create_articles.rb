class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :title
      t.string :slug, index: true, uniq: true
      t.string :description
      t.string :url
      t.string :kind 
      t.integer :status
      t.integer :order
      t.integer :page_id
      t.integer :feature
      t.datetime :published_at
      t.attachment :image
      t.timestamps
    end
  end
end
