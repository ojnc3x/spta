class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.references :image_element
      t.string :title
      t.string :description
      t.attachment :image
      t.timestamps
    end
  end
end
