class CreateImageElements < ActiveRecord::Migration[5.0]
  def change
    create_table :image_elements do |t|
      t.references :image_elementable, polymorphic: true, index: {:name => "image_elementable"}
      t.string :title
      t.string :columns
      t.integer :order
      t.timestamps
    end
  end
end
