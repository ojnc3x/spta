class CreateDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :documents do |t|
      t.references :document_element
      t.string :title
      t.string :description
      t.attachment :file
      t.timestamps
    end
  end
end
