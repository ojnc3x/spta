class AddFieldsToElementableTables < ActiveRecord::Migration[5.0]
  def change
    add_column :image_elements, :description, :text
    add_column :document_elements, :description, :text
    add_column :video_elements, :description, :text
    add_column :audio_elements, :description, :text

    add_attachment :image_elements, :image
    add_attachment :video_elements, :file
    add_attachment :audio_elements, :file
    add_attachment :document_elements, :file

    add_column :document_elements, :url, :string
    add_column :video_elements, :url, :string
    add_column :audio_elements, :url, :string

    add_column :image_elements, :gallery, :boolean
    add_column :document_elements, :gallery, :boolean
    add_column :video_elements, :gallery, :boolean
    add_column :audio_elements, :gallery, :boolean

  end
end
