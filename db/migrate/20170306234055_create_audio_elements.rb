class CreateAudioElements < ActiveRecord::Migration[5.0]
  def change
    create_table :audio_elements do |t|
      t.references :audio_elementable, polymorphic: true, index: {:name => "audio_elementable"}
      t.string :title
      t.string :columns
      t.integer :order
      t.timestamps
    end
  end
end
