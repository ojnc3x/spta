class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title
      t.string :slug, index: true, uniq: true
      t.text :place
      t.text :activity
      t.datetime :event_at
      t.timestamps
    end
  end
end
