class CreateBanners < ActiveRecord::Migration[5.0]
  def change
    create_table :banners do |t|
      t.string :title
      t.string :description
      t.string :url
      t.attachment :image
      t.timestamps
    end
  end
end
