class CreateVideos < ActiveRecord::Migration[5.0]
  def change
    create_table :videos do |t|
      t.references :video_element
      t.string :title
      t.string :description
      t.string :url
      t.attachment :file
      t.timestamps
    end
  end
end
