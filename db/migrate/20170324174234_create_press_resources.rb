class CreatePressResources < ActiveRecord::Migration[5.0]
  def change
    create_table :press_resources do |t|
      t.string :title
      t.string :slug, index: true, uniq: true
      t.string :description
      t.integer :order
      t.timestamps
    end
  end
end
