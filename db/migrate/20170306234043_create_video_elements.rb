class CreateVideoElements < ActiveRecord::Migration[5.0]
  def change
    create_table :video_elements do |t|
      t.references :video_elementable, polymorphic: true, index: {:name => "video_elementable"}
      t.string :title
      t.string :columns
      t.integer :order
      t.timestamps
    end
  end
end
