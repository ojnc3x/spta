class CreatePages < ActiveRecord::Migration[5.0]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug, index: true, uniq: true
      t.string :description
      t.integer :order
      t.integer :page_id
      t.timestamps
    end
  end
end
