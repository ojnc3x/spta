class CreateDocumentElements < ActiveRecord::Migration[5.0]
  def change
    create_table :document_elements do |t|
      t.references :document_elementable, polymorphic: true, index: {:name => "document_elementable"}
      t.string :title
      t.string :columns
      t.integer :order
      t.timestamps
    end
  end
end
