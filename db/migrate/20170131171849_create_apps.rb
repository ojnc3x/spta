class CreateApps < ActiveRecord::Migration[5.0]
  def change
    create_table :apps do |t|
      t.string :title
      t.string :slug, index: true, uniq: true
      t.string :description
      t.string :url
      t.string :kind
      t.integer :order
      t.integer :feature
      t.datetime :published_at
      t.attachment :image
      t.timestamps
    end
  end
end
