class AddActiveToBanners < ActiveRecord::Migration[5.0]
  def change
    add_column :banners, :active, :boolean, :default=>false
    add_column :banners, :radio_stream, :boolean, :default=>false
  end
end
