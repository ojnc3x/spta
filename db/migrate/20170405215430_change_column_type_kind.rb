class ChangeColumnTypeKind < ActiveRecord::Migration[5.0]
  def change
    change_column :articles, :kind, 'integer USING CAST(kind AS integer)'
    change_column :apps, :kind, 'integer USING CAST(kind AS integer)'
  end
end
