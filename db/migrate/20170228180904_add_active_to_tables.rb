class AddActiveToTables < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :active, :integer, :default=>0
    add_column :projects, :active, :integer, :default=>0
    add_column :articles, :active, :integer, :default=>0
    add_column :apps, :active, :integer, :default=>0
    add_column :events, :active, :integer, :default=>0
  end
end
