module ApplicationHelper

  def draw_side_menu
    menu = ""
    menu << "<ul class='vertical menu hide-for-small-only' 'data-accordion' data-allow-all-closed >"
    menu << "<li>"
    menu << "<a href='#'>MENU</a>"
    menu << "<ul class='menu vertical nested is-active'><li><a href='#'>ITEM 1</a></li><li><a href='#'>ITEM 2</a></li></ul>"
    menu << "</li>"
    menu << "<li>"
    menu << "<a href='#'>MENU 2</a>"
    menu << "<ul class='menu vertical nested is-active'><li><a href='#'>IT2EM 1</a></li><li><a href='#'>ITEM 2</a></li></ul>"
    menu << "</li>"
    menu << "</ul>"
    menu.html_safe
  end

  def draw_elements elements=nil, galleries=nil
    html = ""
    elements.each do |element|
      if(element.has_attribute?('gallery') && element.gallery?)
        meth = :"draw_gallery"
        html << send(meth, element, galleries)
      else
        meth = :"draw_#{element.class.to_s.downcase}"
        html << send(meth, element)
      end

    end
    html.html_safe
  end

  def draw_gallery element=nil, galleries=nil
    html = ""
    html << %(<script type='text/javascript'>)
    html << "
      if($('.gallery-element-#{element.order}').length){
        $('.gallery-element-#{element.order}').remove();
      }
    "
    html << %(</script>)
    html << %(<div class="image_element" id="category-image#{element.id}">)
    html << %(<ul class="gallery-element-#{element.order} justified-gallery">)
    galleries.each do |pic|
      html << %(<li data-thumb="#{pic.image.url}" "data-src"="#{pic.image.url}" :"data-sub-html"="<h4>#{pic.title}</h4><p>#{pic.description}</p>">)
      html << image_tag(pic.image.url)
      html << %(</li>)
    end
    html << %(</ul>)
    html << %(</div>)
    html << %(<script type='text/javascript'>)
    html << "
      $('.gallery-element-#{element.order}').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        thumbItem: 9,
        slideMargin: 0,
        enableDrag: false,
        currentPagerPosition:'left',
        //onSliderLoad: function(el) {
        //  el.lightGallery({
        //    selector: '.gallery-element-#{element.order} .lslide'
        //  });
        //},
      });
    "
    html << %(</script>)

  end

  def draw_element(element)
    html = ""
    unless element.title.blank?
      html << %(<h2>)
      html << element.title
      html << %(</h2>)
    end
    html << %(<div class="clear"></div>)
    html << draw_content(element.content)
    html << %(<div class="clear"></div>)
    html.html_safe
  end

  def draw_imageelement(element)
=begin
%ul.gallery-element.justified-gallery
  =# @gallery.each do |photo|
  %li{:"data-thumb"=>"site/fotos-01.png", :"data-src"=>"site/fotos-01.png", :"data-sub-html"=>"<h4>Titulo</h4><p>Descripcion</p>"  }
    = image_tag("site/fotos-01.png")
  %li{:"data-thumb"=>"site/fotos-01.png", :"data-src"=>"site/fotos-01.png", :"data-sub-html"=>"<h4>Titulo</h4><p>Descripcion</p>"  }
    = image_tag("site/fotos-02.png")
  %li{:"data-thumb"=>"site/fotos-01.png", :"data-src"=>"site/fotos-01.png", :"data-sub-html"=>"<h4>Titulo</h4><p>Descripcion</p>"  }
    = image_tag("site/fotos-03.png")
  %li{:"data-thumb"=>"site/fotos-01.png", :"data-src"=>"site/fotos-01.png", :"data-sub-html"=>"<h4>Titulo</h4><p>Descripcion</p>"  }
    = image_tag("site/fotos-01.png")
  %li{:"data-thumb"=>"site/fotos-01.png", :"data-src"=>"site/fotos-01.png", :"data-sub-html"=>"<h4>Titulo</h4><p>Descripcion</p>"  }
    = image_tag("site/fotos-02.png")

=end

    html = ""
    html << %(<div class="image_element" id="category-image#{element.id}">)
    html << %(<img src='#{element.image.url}' />)
    html << %(<h5>#{element.title}</h5>)
    html << %(<span>#{element.description}</span>)
    html << %(</div>)

    html.html_safe
  end

  def draw_content(content="")
    return "" unless content
    return content.html_safe
  end

  def draw_documentelement(element)
    html = ""
    if element.file.present?
      html << %(<div class="row">)
      html << %(<div class="medium-12 columns">)
      html << %(<div class="div-row">)
      html << %(<p>)
      html << image_tag("site/doc.png")
      html << link_to(element.title, element.file.url, target: "_blank")
      html << %(</p>)
      html << %(<p>)
      html << number_to_human_size(element.file_file_size, precision: 2)
      html << %(</p>)
      html << %(</div>)
      unless element.title.blank?
        html << %(<p>)
        html << element.description
        html << %(</p>)
      end
      html << %(</div>)
    end
    html << %(</div>)
    html << %(<div class="clear"></div>)
    html.html_safe
  end

  def draw_videoelement(element)
    html = ""
    unless element.title.blank?
      html << %(<h2>)
      html << element.title
      html << %(</h2>)
    end
    unless element.url.nil?
      html << %(<div class="embed-container">)
      html << YoutubeEmbed.youtube_embed_url(element.url, nil, nil)
      html << %(</div>)
    end
    html << %(<div class="clear"></div>)
    html.html_safe

  end

end
