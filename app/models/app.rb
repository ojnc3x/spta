class App < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  scope :active, -> { where(active: true) }

  has_attached_file :image,
                    styles: {
                      small:  '200x113#',
                      medium: '552x311#',
                      large:  '1140x641#'
                    },
                    default_url: 'site/ga.png'

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}

  enum kind: [:website, :mobile]

  def self.kinds_attributes_for_select
    kinds.map do |kind, _|
      [I18n.t("activerecord.attributes.#{model_name.i18n_key}.kinds.#{kind}"), kind]
    end
  end

  def display_kind
    I18n.t("activerecord.attributes.app.kinds.#{kind}", default: status.titleize)
  end

  has_many :elements, as: :elementable
  has_many :image_elements, as: :image_elementable
  has_many :video_elements, as: :video_elementable
  has_many :audio_elements, as: :audio_elementable
  has_many :document_elements, as: :document_elementable

  accepts_nested_attributes_for :elements, allow_destroy: true
  accepts_nested_attributes_for :video_elements, allow_destroy: true
  accepts_nested_attributes_for :image_elements, allow_destroy: true
  accepts_nested_attributes_for :audio_elements, allow_destroy: true
  accepts_nested_attributes_for :document_elements, allow_destroy: true

end
