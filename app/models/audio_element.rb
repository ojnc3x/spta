class AudioElement < ApplicationRecord
  belongs_to :audio_elementable, polymorphic: true, optional: true

  has_attached_file :file
  do_not_validate_attachment_file_type :file

end
