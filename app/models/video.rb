class Video < ApplicationRecord

  belongs_to :video_element

  has_attached_file :file

end
