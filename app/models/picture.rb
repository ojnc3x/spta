class Picture < ApplicationRecord

  belongs_to :image_element

  has_attached_file :image,
                    styles: {
                      small:  '200x113#',
                      medium: '552x311#',
                      large:  '1140x641#'
                    },
                    default_url: 'site/ga.png'

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}

end
