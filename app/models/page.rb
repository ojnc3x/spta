class Page < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  has_many :pages, -> { order('created_at ASC') }
  belongs_to :page

  has_many :projects

  scope :active, -> { where(active: true) }

  has_many :elements, as: :elementable, dependent: :destroy
  has_many :image_elements, as: :image_elementable, dependent: :destroy
  has_many :video_elements, as: :video_elementable, dependent: :destroy
  has_many :audio_elements, as: :audio_elementable, dependent: :destroy
  has_many :document_elements, as: :document_elementable, dependent: :destroy

  accepts_nested_attributes_for :elements, allow_destroy: true
  accepts_nested_attributes_for :video_elements, allow_destroy: true
  accepts_nested_attributes_for :image_elements, allow_destroy: true
  accepts_nested_attributes_for :audio_elements, allow_destroy: true
  accepts_nested_attributes_for :document_elements, allow_destroy: true

end
