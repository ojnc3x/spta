class Banner < ApplicationRecord

  has_attached_file :image,
                    styles: {
                      small:  '200x113#',
                      medium: '1200x500'
                    },
                    default_url: 'site/ga.png'

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}

end
