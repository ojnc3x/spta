class Attachment < ApplicationRecord
  belongs_to :attachment_category

  validates :downloads, numericality: { only_integer: true }, allow_nil: true

  has_attached_file :datafile

  has_attached_file :thumbnail,
    content_type: ['image/jpeg', 'image/jpg', 'image/png'],
    styles: { thumb: '120x120>' }

  validates_attachment_content_type :thumbnail, content_type: %r{\Aimage\/.*\Z}

  validates_attachment_presence :datafile


end
