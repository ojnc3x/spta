class VideoElement < ApplicationRecord
  belongs_to :video_elementable, polymorphic: true, optional: true

  has_attached_file :file
  do_not_validate_attachment_file_type :file
  
end
