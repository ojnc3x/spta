class DocumentElement < ApplicationRecord
  belongs_to :document_elementable, polymorphic: true, optional: true

  has_attached_file :file
  do_not_validate_attachment_file_type :file

end
