class Element < ApplicationRecord

  belongs_to :elementable, polymorphic: true

  has_many :videos,
           -> { order(:priority) }
  accepts_nested_attributes_for :videos, allow_destroy: true

  has_many :audios,
           -> { order(:priority) }
  accepts_nested_attributes_for :audios, allow_destroy: true

end
