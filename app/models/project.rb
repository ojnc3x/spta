class Project < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: [:slugged, :finders]

  scope :active, -> { where(active: true) }

  has_attached_file :image,
                    styles: {
                      small:  '371x210#',
                      medium: '552x311#',
                      large:  '1140x641#'
                    },
                    default_url: 'site/ga.png'

  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}

  enum direction: [:participacion, :transparencia, :ofcia, :administracion, :comunicaciones_y_tecnologia]

  def direction_name
    return nil if self.direction.nil?
    I18n.t("activerecord.enum.project.direction.#{self.direction}")
  end

  has_many :elements, as: :elementable
  has_many :image_elements, as: :image_elementable
  has_many :video_elements, as: :video_elementable
  has_many :audio_elements, as: :audio_elementable
  has_many :document_elements, as: :document_elementable

  accepts_nested_attributes_for :elements, allow_destroy: true
  accepts_nested_attributes_for :video_elements, allow_destroy: true
  accepts_nested_attributes_for :image_elements, allow_destroy: true
  accepts_nested_attributes_for :audio_elements, allow_destroy: true
  accepts_nested_attributes_for :document_elements, allow_destroy: true

end
