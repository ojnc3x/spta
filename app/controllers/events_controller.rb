class EventsController < ApplicationController
  include Componentable
  layout 'pages'

  def model
    Event
  end

  def index
    @events = Event.where("event_at >= ?", Date.today.beginning_of_day)
  end

  def init_breadcrumb
    super
    add_breadcrumb "Eventos", events_url()
  end

end
