class EmblematicCasesController < ApplicationController
  include Componentable
  layout 'pages'

  def model
    Article
  end

  def index
    @articles = Article.where(active: 1, kind: [:emblematic]).order("feature ASC, created_at ASC")
  end

  def show
    prepare_data
    @articles = Article.limit(1)
    @item = model.friendly.active.find(params[:id])
    unless @item.emblematic?
      redirect_to article_url(@item)
    end
    add_breadcrumb @item.title, emblematic_case_url(@item)
  end

  def init_breadcrumb
    super
    add_breadcrumb "Casos emblemáticos", emblematic_cases_url()
  end

end
