class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :header_and_side_content

  def header_and_side_content
    @first_page = Page.first
    @side_article = Article.where(feature: 1, active: 1).order("created_at DESC").first rescue nil
    @side_app = App.where(feature: 1, active: 1).order("created_at DESC").first rescue nil
  end
end
