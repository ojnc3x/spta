class Admin::ArticlesController < Admin::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    Article
  end

  def item_params
    params.require(:article).permit!
  end

end
