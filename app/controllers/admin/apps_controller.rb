class Admin::AppsController < Admin::ApplicationController

  def model
    App
  end

  def item_params
    params.require(:app).permit!
  end

end
