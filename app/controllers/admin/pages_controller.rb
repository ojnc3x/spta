class Admin::PagesController < Admin::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    Page
  end

  def item_params
    params.require(:page).permit!
    #params.require(:page).permit(:title, :description, :order, :created_at,
    #:page_id, :active, :feature, :image, :kind, :published_at, :url,
    #image_elements_attributes: [:id, :title, :order, :_destroy, new_image_elements: [pictures_attributes: [:title, :description, :image, :_destroy]]]
    #)

  end

end
