class Admin::BannersController < Admin::ApplicationController

  def model
    Banner
  end

  def index
    @items = model.where(conditions)
    add_breadcrumb model.model_name.human(count: :many), index_url
  end

  def item_params
    params.require(:banner).permit!
  end

end
