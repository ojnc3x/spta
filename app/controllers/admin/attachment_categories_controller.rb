class Admin::AttachmentCategoriesController < Admin::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    AttachmentCategory
  end

  def index
    @items = model.where(conditions)
    add_breadcrumb model.model_name.human(count: :many), index_url
  end

  def item_params
    params.require(:attachment_category).permit!
  end

  def create
    @item = model.new item_params
    before_controller_create

    if @item.save
      redirect_to url_for(action: :index), notice: t('layouts.admin.notice.created')
    else
      init_form
      add_breadcrumb model.model_name.human(count: :many), index_url
      add_breadcrumb t('layouts.admin.breadcrumb.new')

      render template: 'concerns/tabled/new'
    end
  end

  def update
    @item = model.find params[:id]
    before_controller_update
    if @item.update_attributes item_params
      redirect_to url_for(action: :index), notice: t('layouts.admin.notice.updated')
    else
      init_form
      add_breadcrumb model.model_name.human(count: :many), index_url
      add_breadcrumb t('layouts.admin.breadcrumb.edit')
      render template: 'concerns/tabled/edit'
    end
  end


end
