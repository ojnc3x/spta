class Admin::PressResourcesController < Admin::ApplicationController

  def model
    PressResource
  end

  def item_params
    params.require(:press_resource).permit!
  end

end
