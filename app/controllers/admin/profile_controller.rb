class Admin::ProfileController < Admin::ApplicationController

  def model
    User
  end

  def edit
    @user = model.find current_user.id
    redirect_to admin_root_url if @user != current_user
  end

  def update
    @user = model.find current_user.id
    if @user.update_attributes item_params
      sign_in @user, :bypass => true
      redirect_to url_for(action: :edit, id: @user.id), notice: t('layouts.admin.notice.updated')
    else
      redirect_to url_for(action: :edit, id: @user.id), notice: t('layouts.admin.notice.error')
    end
  end

  def item_params
    params.require(:user).permit(
      :name,
      :lastname,
      :password,
    )
  end

end
