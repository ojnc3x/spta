class Admin::ProjectsController < Admin::ApplicationController
  protect_from_forgery with: :exception, unless: -> { request.format.json? }

  def model
    Project
  end

  def item_params
    params.require(:project).permit!
  end

end
