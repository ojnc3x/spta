class ProjectsController < ApplicationController
  include Componentable
  layout 'pages'

  def model
    Project
  end

  def index
    @projects = Project.where(active: 1).order("feature ASC, created_at ASC")
  end

  def show
    @projects = Project.where(active: 1).order("feature ASC, created_at ASC")
    super
  end

  def init_breadcrumb
    super
    add_breadcrumb "Proyectos", projects_url()
  end

end
