class ArticlesController < ApplicationController
  include Componentable
  require 'youtube_embed'
  layout 'pages'

  def model
    Article
  end

  def index
    @articles = Article.where(active: 1, kind: [:article, :video]).order("feature ASC, created_at ASC")
  end

  def show
    prepare_data
    @articles = Article.limit(1)
    @item = model.friendly.active.find(params[:id])
    puts @item.kind
    if @item.emblematic?
      redirect_to emblematic_case_url(@item)
    end

  end

  def init_breadcrumb
    super
    add_breadcrumb "Noticias", articles_url()
  end

end
