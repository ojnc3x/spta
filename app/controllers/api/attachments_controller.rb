class Api::AttachmentsController < ApiController

  def download

    if Attachment.exists? params[:id]

      attachment = Attachment.find params[:id]
      attachment.update_attributes(downloads: (attachment.downloads + 1))
      send_file attachment.datafile.path,
                filename: [attachment.name, File.extname(attachment.datafile.path)].join,
                type: attachment.datafile_content_type,
                disposition: 'attachment'
    else
      render_404
    end
  end

end
