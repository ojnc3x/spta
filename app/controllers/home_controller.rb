class HomeController < ApplicationController
  layout "application"

  def index
    @banner = Banner.where(active: true).first
    @articles = Article.where(feature: 1, active: 1, kind: "article").limit(3)
    @projects = Project.where(feature: 1, active: 1).limit(3)
    @emblematic_case = Article.where(feature: 1, active: 1, kind: "emblematic").first
    @attachment = Attachment.last
    @app = App.first
    @press_resource = PressResource.last rescue nil
  end

end
