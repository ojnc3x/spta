class AppsController < ApplicationController
  include Componentable
  layout 'pages'

  def model
    App
  end

  def index
    @apps = model.active.order("title ASC")
  end

end
