class PagesController < ApplicationController
  include Componentable

  def model
    Page
  end

  def show
    @pages = Page.active
    super
  end

end
