class PressResourcesController < ApplicationController
  include Componentable

  layout 'pages'

  def model
    PressResource
  end

  def index
    @press_resources = PressResource.all
  end

  def show
    prepare_data
    @press_resources = PressResource.limit(1)
    @item = model.friendly.find(params[:id])

    add_breadcrumb "Inicio", index_url
    add_breadcrumb t('layouts.admin.breadcrumb.show')

  end
end
