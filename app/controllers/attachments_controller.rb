class AttachmentsController < ApplicationController
  include Componentable

  layout 'pages'

  def model
    Attachment
  end

  def index
    @attachment_categories = AttachmentCategory.all
    #@attachments = Attachment.all
  end

  def show
    @attachment_category = AttachmentCategory.find(params[:id])
    @attachments = @attachment_category.attachments
    add_breadcrumb @attachment_category.name, attachment_url(@attachment_category)
  end


    def init_breadcrumb
      super
      add_breadcrumb "Informes y descargas", attachments_url()
    end


end
